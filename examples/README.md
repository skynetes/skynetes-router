# `/examples`


# First goal here is to get routing working using standard Go (prototype)

this is an example router using the https://github.com/go-playground/lars router


Idea is to test out integration with ETCD in this module

1. take incoming request for basic service 
2. inspect payload
3. route to another url based on payload


go get go.etcd.io/etcd/client

https://github.com/coreos/etcd-operator/blob/master/doc/user/install_guide.md



#Next phase will be to optimize and integrate with exiting products


Ambassador is a Kubernetes-native API gateway for microservices. 
Ambassador is deployed at the edge of your network, 
and routes incoming traffic to your internal services (aka "north-south" traffic). 
Istio is a service mesh for microservices, and is designed to add application-level Layer (L7) observability, 
routing, and resilience to service-to-service traffic (aka "east-west" traffic). 
Both Istio and Ambassador are built using Envoy.


# Envoy
HTTP L7 routing: When operating in HTTP mode, Envoy supports a routing subsystem that is capable of routing and redirecting requests based on path, authority, content type, runtime values, etc. This functionality is most useful when using Envoy as a front/edge proxy but is also leveraged when building a service to service mesh.

gRPC support: gRPC is an RPC framework from Google that uses HTTP/2 as the underlying multiplexed transport. Envoy supports all of the HTTP/2 features required to be used as the routing and load balancing substrate for gRPC requests and responses. The two systems are very complementary.